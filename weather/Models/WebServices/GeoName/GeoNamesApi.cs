﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace weather.Models.WebServices
{
    public class GeoNamesApi
    {
        public List<City> cities;

        public GeoNamesApi()
        {
            // Empty
        }
   
        public List<City> Search(String cityName)
        {

           var url = "http://api.geonames.org/search?name=" + cityName + "&country=SE&type=json&username=jensaronsson";


            WebClient client = new WebClient();

            var stream = client.OpenRead(url);

            StreamReader sr = new StreamReader(stream);

            var ret = sr.ReadToEnd();
            

            var t = JObject.Parse(ret);

            stream.Close();

            cities = t["geonames"]
                .Select(u => new City 
                { 
                    Name = (string)u["toponymName"],
                    State = (string)u["adminName1"],
                    IdentCode = (string)u["fcode"],
                    GeoNameId = (int)u["geonameId"]
                }).Where(u => u.IdentCode.StartsWith("PPL") || u.IdentCode.StartsWith("PPLC") || u.IdentCode.StartsWith("PPL"))
                .ToList();

            return cities;
        }

    

    }
}