﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace weather.Models.WebServices.Yr
{
    public class YrApi
    {
        private List<Weather> _weatherList;
  


        public List<Weather> WeatherList
        {
            get
            {
                return _weatherList.OrderBy(u => u.DateFrom).Take(5).OrderBy(u => u.DateFrom).ToList();

            }
        }

        

        public YrApi(string city, string state)
        {
            _weatherList = new List<Weather>();


            var url = "http://www.yr.no/place/Sweden/" + state + "/" + city + "/forecast.xml";
           

            var t = XDocument.Load(url);

            var forecast = t.Element("weatherdata").Element("forecast");
            var nextUpdate = t.Element("weatherdata").Element("meta").Element("nextupdate").Value;
            var geobaseid = t.Element("weatherdata").Element("location").Element("location").Attribute("geobaseid").Value;
            


            var data = forecast.Element("tabular").Elements("time");


            var weatherList = data.Select(i => new Weather
            {
                Period = (string)i.Attribute("period"),
                DateFrom = ParseDateString((string)i.Attribute("from")),
                DateEnd = ParseDateString((string)i.Attribute("to")),
                NextUpdate = ParseDateString(nextUpdate),
                GeoBaseId = int.Parse(geobaseid),
                Temp = (string)i.DescendantsAndSelf().Elements("temperature").Attributes("value").FirstOrDefault().Value,
                Symbol = (string)i.DescendantsAndSelf().Elements("symbol").Attributes("number").FirstOrDefault().Value
            }).ToList();

           

            if (weatherList.First().Period.Equals("2") || weatherList.First().Period.Equals("3") )
            {
                _weatherList.Add(weatherList.First());
                weatherList.Remove(weatherList.First());
            }

            /* This LINQ expression work but, sorts the dates wrong */
            /*
            var n = 1;
            weatherList.RemoveAll(u => u.Period != "2");
            weatherList.ForEach(x => x.id = n++);
            _weatherList.AddRange(weatherList); */



            var k = sort(weatherList);
            while (k.MoveNext())
            {
                _weatherList.Add(k.Current);
            }

            

        }


        public IEnumerator<Weather> sort(List<Weather> weatherdata)
        {
            // Need to give weather an unique id so that entity framework wont start to cry.
            var i = 1;
            foreach (var weather in weatherdata)
            {
                if (weather.Period.Equals("2"))
                {
                    weather.id = i++;
                    yield return weather;
                }

            }

        }

        public DateTime ParseDateString(string dateString)
        {
            return DateTime.ParseExact(dateString, "yyyy'-'MM'-'dd'T'HH:mm:ss", new CultureInfo("sv-SE"));
        }
    }
}
