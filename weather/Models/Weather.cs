﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace weather.Models
{
    [MetadataType(typeof(Weather_Metadata))]
    public partial class Weather
    {

        public string cssSymbol { get { return GetSymbolCssText(Symbol); } }
        public string GetSymbolCssText(string symbolNumber)
        {
            string[] cssClas = { "null", "wi-day-sunny", "wi-day-cloudy", "wi-day-cloudy", "wi-cloudy",
                                   "wi-day-sprinkle", "wi-day-thunderstorm", "wi-day-rain-mix",
                                   "wi-day-snow", "wi-showers", "wi-rain", "wi-storm-showers",
                                   "wi-rain-mix", "wi-snow", "wi-snow", "wi-fog", "wi-day-sunny",
                                   "wi-day-cloudy", "wi-day-showers", "wi-day-snow", "wi-day-storm-showers",
                                   "wi-day-storm-showers", "wi-day-storm-showers", "wi-day-storm-showers"  };



            return cssClas[int.Parse(symbolNumber)];

        }
    }

    class Weather_Metadata
    {
        private string _symbol;
        

        public string GeoBaseId { get; set; }

        public DateTime NextUpdate { get; set; }
        public string Period { get; set; }
        
        public DateTime DateFrom { get; set; }

        public DateTime DateEnd { get; set; }

        public string Temp { get; set; }

        public string Symbol { get; set; }


       


    }
}