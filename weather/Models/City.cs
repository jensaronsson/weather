﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace weather.Models
{
    [MetadataType(typeof(City_Metadata))]
    public partial class City
    {
        public bool HasWeather 
        {
            get 
            {
                return this.Weathers.Any();
            }
        }

        class City_Metadata
        {

            public int GeoNameId { get; set; }

            public string Name { get; set; }

            public string State { get; set; }

            public string IdentCode { get; set; }
            

        }

    }

  
}