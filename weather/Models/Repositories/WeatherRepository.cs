﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using weather.Models.DataModels;

namespace weather.Models.Repositories
{
    public class WeatherRepository  : IWeatherRepository
    {
        private weatherEntities2 _entities = new weatherEntities2();


        public  IEnumerable<City> Cities(string name)
        {

            return _entities.Cities.Where(u => u.Name.Contains(name)).AsEnumerable();
        }

        public City City(int geoNameId)
        {
            return _entities.Cities.Where(u => u.GeoNameId == geoNameId).FirstOrDefault();
        }

        public void Save()
        {
            _entities.SaveChanges();
        }


        public void Add(City city)
        {
            
            _entities.Cities.Add(city);
        }

        
        public void Update(City city)
        {
            _entities.Entry(city).State = EntityState.Modified;
        }

        public void RemoveWeather(Weather weather)
        {
            //var w = _entities.Weathers.Find(weather.CityId);
            _entities.Weathers.Remove(weather);
        }

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _entities.Dispose();
                }
            }
            this._disposed = true;
        }


    }
}