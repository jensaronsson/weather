﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace weather.Models.Repositories
{
    public interface IWeatherRepository : IDisposable
    {
         void Add(City city);
         IEnumerable<City> Cities(string name);
         City City(int geoNameId);
         void Update(City city);
         void RemoveWeather(Weather weather);
         void Save();


    }
}