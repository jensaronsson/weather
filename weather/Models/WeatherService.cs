﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using weather.Models.Repositories;
using weather.Models.WebServices;
using weather.Models.WebServices.Yr;

namespace weather.Models
{
    public class WeatherService
    {
        private IWeatherRepository _repository;


        public WeatherService()
            : this(new WeatherRepository())
        {
            // Empty
        }


        public WeatherService(IWeatherRepository repo)
        {
            _repository = repo; 
        }


        public IEnumerable<City> SearchCities(string name)
        {
            var cities = _repository.Cities(name);
            
            if (cities.Count() == 0)
            {
                cities = GeoName.Search(name);

                if (cities.Count() == 0)
                {
                    throw new Exception("Kunde inte hitta stad");
                }

                var i = 0;
                foreach (var city in cities)
                {
                    city.id = i++;
                    _repository.Add(city);
                }
                _repository.Save();

            }

            return cities;
        }




        public City GetWeatherFromId(int geoNameId)
        {
            var city = _repository.City(geoNameId);

            if (city == null)
            {
                throw new Exception("Kunde inte hitta stad");
            }

            if (!city.HasWeather || city.Weathers.FirstOrDefault().NextUpdate < DateTime.Now)
            {
                // remove old weather data.
                city.Weathers.ToList().ForEach(u => _repository.RemoveWeather(u));

                // Search and add new weather data.
                Yr.Search(city.Name, city.State).WeatherList.ForEach(u => city.Weathers.Add(u));
               _repository.Update(city); 

            }

            _repository.Save();

            return city;

        }
    }
}