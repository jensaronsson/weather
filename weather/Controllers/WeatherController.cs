﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using weather.Models;
using weather.Models.Repositories;
using weather.Models.WebServices;
using weather.Models.WebServices.Yr;
using weather.ViewModels;

namespace weather.Controllers
{
    public class WeatherController : Controller
    {
        //
        // GET: /
        private WeatherService _service = new WeatherService();


    
        public ActionResult Index()
        {
           
            return View("Index");
        }

        [HttpPost, ActionName("Index")]
        [ValidateAntiForgeryToken]
        public ActionResult search([Bind(Include="SearchName")] WeatherViewModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    model.Cities = _service.SearchCities(model.SearchName);

                    if (model.Cities.Count() == 1)
                    {
                        return Redirect("/" + model.Cities.First().GeoNameId + "/" + model.Cities.First().State + "/" + model.Cities.First().Name);
                    }

                }
            }
            catch (Exception e)
            {
                
                ModelState.AddModelError(String.Empty, e.Message);
            }
            

            return View("Index", model);
        }




        public ActionResult weather([Bind(Include = "Geonameid, State, Name")] WeatherViewModel model)
        {
            ModelState.Remove("SearchName");
            try
            {

                if (ModelState.IsValid)
                {
                    model.City = _service.GetWeatherFromId(model.GeoNameId);
                }

                return View("Index", model);
            }
            catch (Exception e)
            {
                //throw e;
                ModelState.AddModelError(String.Empty, e.Message);
            }
            return View("Index", model);

        }


        
    }
}
