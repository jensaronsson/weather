﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace weather
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "WeatherRoute",
                url: "{geonameid}/{state}/{name}",
                defaults: new { controller = "weather", action = "weather", geonameid = UrlParameter.Optional, state = UrlParameter.Optional, name = UrlParameter.Optional },
                constraints: new { geonameid = @"[0-9]+" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "weather", action = "Index", id = UrlParameter.Optional }
            );



        }
    }
}