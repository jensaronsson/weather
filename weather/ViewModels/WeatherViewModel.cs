﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using weather.Models;

namespace weather.ViewModels
{
    public class WeatherViewModel
    {


        public string State { get; set; }
        public int GeoNameId { get; set; }
        
        public City City { get; set; }
        public IEnumerable<City> Cities { get; set; }


        public bool HasCities { get { return Cities != null && Cities.Count() > 1; } }
        public bool HasWeather { get { return !HasCities; } }
        



        [Display(Name="Sök på ort")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Sökordet måste var minst på 3 tecken!")]
        [Required(ErrorMessage="Du måste ange en ort")]
        public string SearchName { get; set; }

        public string Name { get; set; }
    }
}